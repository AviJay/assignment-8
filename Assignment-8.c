#include<stdio.h>
int no_of_students;
int a;

struct Student  
{  
    char Name[20];  
    char Subject[20];  
    float Marks;  
};  

int main(){

//User inputs the number of students needed//
printf("Enter the Number of Students Needed: ");
scanf("%d",&no_of_students);

//Checks whether the number of students is less than 5 //
while(no_of_students<5)
{
	printf("Enter at least 5 Students");
	scanf("%f",&no_of_students);
}

struct Student student[no_of_students];

//Recording the information//
for(a=0;a<no_of_students;a++){
	printf("Enter the Name of the student: ");
	scanf("%s",student[a].Name);
	printf("Enter the Subject: ");
	scanf("%s",student[a].Subject);
	printf("Enter the marks obtained for the subject: ");
	scanf("%f",&student[a].Marks);
}
 
printf("-------------------------------------------------------------------------\n");

//Displaying the information//
for (a=0; a<no_of_students; a++){
       printf ("Name of the Student: " );
       printf (student[a].Name);
       printf ("\nSubject: ");
       printf (student[a].Subject);
       printf ("\nMarks: %0.2f\n", student[a].Marks);
       printf("\n");

   }
return 0;
}